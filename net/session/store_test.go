package sessions

import (
	"fmt"

	"gitee.com/go-better/dev/net/session/cookiestore"
)

func ExampleStore_assertCookieStore() {
	var cs interface{} = cookiestore.New("")
	if _, ok := cs.(Store); ok {
		fmt.Println("ok")
	}
	// Output: ok
}
