## 账号
用户、公司、员工、角色、权限
### 　用户
#### 　　[用户列表 ： GET /users](GET_users.md)
#### 　　[用户详情 ： GET /users/(?P&lt;type&gt;\w+)/(?P&lt;id&gt;\d+)](users/grUHMHMwKlkYAavz-Oif3w/GET_P5wb8cYuHWKc8sbkz-m1vQ.md)
### 　[公司 (/companies)](companies/README.md)
## [商品 (/goods)](goods/README.md)
## [单据 (/bill)](bill/README.md)
## [库存 (/storage)](storage/README.md)
