package email

import (
	"bytes"
	"errors"
	"fmt"
)

const (
	MIMEVersion             = "MIME-Version"
	ContentType             = "Content-Type"
	ContentTransferEncoding = "Content-Transfer-Encoding"
	ContentDisposition      = "Content-Disposition"
	ContentDescription      = "Content-Description"

	From = "From"
	To   = "To"
	Cc   = "Cc"
	Bcc  = "Bcc"

	Subject = "Subject"
)

func getRecipients(headers []Header) (recipients []string, err error) {
	for _, h := range headers {
		if len(h.Values) == 0 {
			continue
		}
		switch h.Name {
		case To, Cc, Bcc:
			if addressList, err := h.parseAddressList(); err != nil {
				return nil, err
			} else {
				recipients = append(recipients, addressList...)
			}
		}
	}
	return
}

func validateHeaders(headers []Header) error {
	for _, h := range headers {
		if err := h.validate(); err != nil {
			return err
		}
	}
	return nil
}

func setupMIMEVersion(headers []Header) []Header {
	if h := findHeader(headers, MIMEVersion); h.isEmpty() {
		headers = append(headers, Header{Name: MIMEVersion, Values: []string{"1.0"}})
	}
	return headers
}

func setupEntityHeaders(headers []Header, p *properties) ([]Header, error) {
	headers, err := setupContentType(headers, p)
	if err != nil {
		return nil, err
	}
	headers, err = setupContentTransferEncoding(headers, p)
	if err != nil {
		return nil, err
	}
	return headers, nil
}

var contentTypeError1 = errors.New("email: Message is multipart, but Content-Type is not multipart.")
var contentTypeError2 = errors.New("email: Message is not multipart, but Content-Type is multipart.")

func setupContentType(headers []Header, p *properties) ([]Header, error) {
	contentType := findHeader(headers, ContentType)
	if contentType.isEmpty() {
		if p.isMultipart {
			headers = append(headers, Header{Name: ContentType, Values: []string{"multipart/mixed"}})
			contentType = &headers[len(headers)-1]
		}
	} else {
		headerIsMultipart := contentType.isMultipart()
		if p.isMultipart && !headerIsMultipart {
			return nil, contentTypeError1
		}
		if !p.isMultipart && headerIsMultipart {
			return nil, contentTypeError2
		}
	}
	if p.isMultipart {
		contentType.Values[0] += `; boundary="` + p.multipartBoundary + `"`
	}
	return headers, nil
}

func setupContentTransferEncoding(headers []Header, p *properties) ([]Header, error) {
	contentTransferEncoding := findHeader(headers, ContentTransferEncoding)
	if contentTransferEncoding.isEmpty() {
		if !p.isComposite {
			headers = append(headers, Header{Name: ContentTransferEncoding, Values: []string{"base64"}})
			contentTransferEncoding = &headers[len(headers)-1]
		}
	} else if p.isComposite {
		switch contentTransferEncoding.Values[0] {
		case "7bit", "8bit", "binary":
		default:
			// RFC2045: "it is EXPRESSLY FORBIDDEN to use any encodings other than "7bit", "8bit", or "binary"
			// with any composite media type"
			return nil, fmt.Errorf(
				`invalid %s "%s" for a composite media type.`,
				contentTransferEncoding.Name, contentTransferEncoding.Values[0],
			)
		}
	}
	if !contentTransferEncoding.isEmpty() {
		p.contentTransferEncoding = contentTransferEncoding.Values[0]
	}
	return headers, nil
}

func findHeader(headers []Header, name string) *Header {
	for i := range headers {
		if headers[i].Name == name {
			return &headers[i]
		}
	}
	return nil
}

func writeHeadersTo(headers []Header, buf *bytes.Buffer) error {
	for _, h := range headers {
		if err := h.writeTo(buf); err != nil {
			return err
		}
	}
	return nil
}
