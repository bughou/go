package email

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"strings"
)

var testContent = []byte(`我们中国人民从此站立起来了！We Chinese People Stand Up From Now!`)

func ExampleWriteBodyTo_base64() {
	var buf = &bytes.Buffer{}
	if err := writeBodyTo(testContent, properties{}, buf); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(buf.String())
	}
	if b, err := base64.StdEncoding.DecodeString(buf.String()); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(string(b))
	}
	// Output:
	// 5oiR5Lus5Lit5Zu95Lq65rCR5LuO5q2k56uZ56uL6LW35p2l5LqG77yBV2UgQ2hpbmVzZSBQZW9w
	// bGUgU3RhbmQgVXAgRnJvbSBOb3ch
	//
	// 我们中国人民从此站立起来了！We Chinese People Stand Up From Now!
}

func ExampleWriteBodyTo_quotedprintable() {
	var buf = &bytes.Buffer{}
	if err := writeBodyTo(testContent, properties{contentTransferEncoding: "quotedprintable"}, buf); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(strings.Replace(buf.String(), "\r\n", "\n", -1))
	}
	// Output:
	// =E6=88=91=E4=BB=AC=E4=B8=AD=E5=9B=BD=E4=BA=BA=E6=B0=91=E4=BB=8E=E6=AD=A4=E7=
	// =AB=99=E7=AB=8B=E8=B5=B7=E6=9D=A5=E4=BA=86=EF=BC=81We Chinese People Stand =
	// Up From Now!
}

func ExampleWriteBodyTo_bit7() {
	var buf = &bytes.Buffer{}
	if err := writeBodyTo(testContent, properties{contentTransferEncoding: "bit7"}, buf); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(buf.String())
	}
	// Output:
	// 我们中国人民从此站立起来了！We Chinese People Stand Up From Now!
}
