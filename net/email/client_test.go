package email

import (
	"context"
	"fmt"
	"net/url"
	"time"
)

func ExampleClient() {
	client, err := NewClient(testUrl)
	if err != nil {
		panic(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// send simple email
	err = client.Send(ctx, &Message{
		Headers: []Header{
			{Name: To, Values: []string{"测试收件人<" + testTo + ">"}},
			{Name: Subject, Values: []string{"测试Simple"}},
			{Name: ContentType, Values: []string{"text/html; charset=utf-8"}},
		},
		Body: []byte("<h1>测试内容</h1>"),
	})
	if err != nil {
		fmt.Println(err)
	}

	// send multipart email
	err = client.Send(ctx, &Message{
		Headers: []Header{
			{Name: To, Values: []string{"测试收件人<" + testTo + ">"}},
			{Name: Subject, Values: []string{"测试Multipart"}},
		},
		BodyParts: []Message{
			{
				Headers: []Header{
					{Name: ContentType, Values: []string{"text/html; charset=utf-8"}},
				},
				Body: []byte(`<h1>测试内容：含附件。</h1>`),
			},
			{
				Headers: []Header{
					{Name: ContentType, Values: []string{"text/plain; charset=utf-8"}},
					{Name: ContentDisposition, Values: []string{`attachment; filename="file.txt"`}},
				},
				Body: []byte("这是附件内容。"),
			},
		},
	})
	if err != nil {
		fmt.Println(err)
	}

	// Output:
}

func ExampleGetUsableFunc() {
	_, err := getUsableFunc(url.Values{"safeIdleTime": []string{"1m"}})
	fmt.Println(err)
	// Output: <nil>
}
