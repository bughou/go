package email

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"mime/quotedprintable"
)

func ExampleMessage() {
	const longContent = "我们中国人民从此站立起来了！We Chinese People Stand Up From Now!"
	m := Message{
		Headers: []Header{
			{Name: From, Values: []string{"测试来源<applejava@qq.com>"}},
			{Name: To, Values: []string{"测试收件人<applejava@qq.com>"}},
			{Name: Subject, Values: []string{"测试主题：" + longContent}},
		},
		Body: []byte("测试内容：" + longContent),
	}
	if recipients, err := getRecipients(m.Headers); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(recipients)
	}
	if body, err := m.Bytes(); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(string(body))
	}

	// Output:
	// [applejava@qq.com]
	// From: =?utf-8?b?5rWL6K+V5p2l5rqQ?= <applejava@qq.com>
	// To: =?utf-8?b?5rWL6K+V5pS25Lu25Lq6?= <applejava@qq.com>
	// Subject: =?utf-8?b?5rWL6K+V5Li76aKY77ya5oiR5Lus5Lit5Zu95Lq65rCR5LuO5q2k56uZ56uL?=
	//  =?utf-8?b?6LW35p2l5LqG77yBV2UgQ2hpbmVzZSBQZW9wbGUgU3RhbmQgVXAgRnJvbSBO?=
	//  =?utf-8?b?b3ch?=
	// MIME-Version: 1.0
	// Content-Transfer-Encoding: base64
	//
	// 5rWL6K+V5YaF5a6577ya5oiR5Lus5Lit5Zu95Lq65rCR5LuO5q2k56uZ56uL6LW35p2l5LqG77yB
	// V2UgQ2hpbmVzZSBQZW9wbGUgU3RhbmQgVXAgRnJvbSBOb3ch
}

func ExampleQuotedPrintable() {
	const input = "\n\r\n\r"

	var buf = &bytes.Buffer{}
	w := quotedprintable.NewWriter(buf)
	w.Write([]byte(input))
	w.Close()
	fmt.Println(buf.Bytes())

	output, err := ioutil.ReadAll(quotedprintable.NewReader(buf))
	fmt.Println(output, err)

	fmt.Println(input == string(output))

	// Output:
	// [13 10 13 10 13 10]
	// [13 10 13 10 13 10] <nil>
	// false
}
