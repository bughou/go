package email

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"io"
	"mime/quotedprintable"
)

func writeBodyTo(body []byte, p properties, buf *bytes.Buffer) error {
	switch p.contentTransferEncoding {
	case "base64", "":
		if err := writeBase64Body(body, buf); err != nil {
			return err
		}
	case "quotedprintable":
		if err := writeQuotedprintableBody(body, buf); err != nil {
			return err
		}
	case "bit7", "bit8", "binary":
		if _, err := buf.Write(body); err != nil {
			return err
		}
	default:
		return fmt.Errorf("Unknown %s: %s", ContentTransferEncoding, p.contentTransferEncoding)
	}
	return buf.WriteByte('\n')
}

func writeBase64Body(body []byte, writer io.Writer) error {
	w := base64.NewEncoder(base64.StdEncoding, NewLineWriter(writer))
	defer w.Close()
	_, err := w.Write(body)
	return err
}

func writeQuotedprintableBody(body []byte, writer io.Writer) error {
	w := quotedprintable.NewWriter(writer)
	defer w.Close()
	_, err := w.Write(body)
	return err
}

// LineWriter add "\n" after every 76 bytes.
type LineWriter struct {
	w io.Writer
	// how many bytes have writtern on the last line which have not added "\n".
	lastLineWrittern int
}

func NewLineWriter(w io.Writer) *LineWriter {
	return &LineWriter{w: w}
}

func (lw *LineWriter) Write(b []byte) (total int, err error) {
	const maxLineLen = 76
	for expect := maxLineLen - lw.lastLineWrittern; len(b) >= expect; {
		n, err := lw.w.Write(b[:expect])
		total += n
		if err != nil {
			return total, err
		}
		if _, err := lw.w.Write([]byte("\n")); err != nil {
			return total, err
		}
		b = b[expect:]
		expect = maxLineLen
		lw.lastLineWrittern = 0
	}

	n, err := lw.w.Write(b)
	total += n
	if err != nil {
		return total, err
	}
	lw.lastLineWrittern += len(b)

	return total, nil
}
