package email

import (
	"bytes"
	"fmt"
	"net/mail"
	"strings"
)

type Header struct {
	Name   string
	Values []string
}

func (h *Header) writeTo(buf *bytes.Buffer) error {
	if h.isEmpty() {
		return nil
	}
	switch h.Name {
	case From:
		if len(h.Values) == 1 {
			return writeHeaderTo(h.Name, h.Values[0], buf)
		}
	case To, Cc, Bcc:
		return writeHeaderTo(h.Name, strings.Join(h.Values, ", "), buf)
	default:
		for _, v := range h.Values {
			return writeHeaderTo(h.Name, v, buf)
		}
	}
	return nil
}

func (h *Header) isMultipart() bool {
	return h.Name == ContentType && len(h.Values) == 1 &&
		strings.HasPrefix(h.Values[0], "multipart/")
}

func (h *Header) isEmpty() bool {
	if h == nil {
		return true
	}
	for _, v := range h.Values {
		if v != "" {
			return false
		}
	}
	return true
}

func (h *Header) parseAddressList() (result []string, err error) {
	for _, v := range h.Values {
		if nameAddrs, err := mail.ParseAddressList(v); err != nil {
			return nil, err
		} else {
			for _, nameAddr := range nameAddrs {
				result = append(result, nameAddr.Address)
			}
		}
	}
	return
}

func (h *Header) validate() error {
	if h.isEmpty() {
		return nil
	}

	switch h.Name {
	case MIMEVersion, ContentType, ContentTransferEncoding, From:
		if len(h.Values) > 1 {
			return fmt.Errorf(`email: "%s" header should have exactly one value.`, h.Name)
		}
	}
	return nil
}

func writeHeaderTo(name, value string, buf *bytes.Buffer) error {
	switch name {
	case From, To, Cc, Bcc:
		if v, err := encodingAddressList(value); err != nil {
			return err
		} else {
			value = v
		}
	case Subject:
		value = BEncoding.Encode("utf-8", value)
	case MIMEVersion, ContentType, ContentTransferEncoding, ContentDisposition:
	}
	return writeLine(name+": "+value, buf)
}

func encodingAddressList(s string) (string, error) {
	addressList, err := mail.ParseAddressList(s)
	if err != nil {
		return "", err
	}
	var result = make([]string, len(addressList))
	for i, addr := range addressList {
		result[i] = fmt.Sprintf(`%s <%s>`, BEncoding.Encode("utf-8", addr.Name), addr.Address)
	}
	return strings.Join(result, ", "), nil
}
