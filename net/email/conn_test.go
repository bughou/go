package email

import (
	"context"
	"fmt"
	"net/smtp"
	"time"
)

const (
	testHost = "smtp.qq.com"
	testPort = "465"
	testFrom = "xiaomei-go@qq.com"
	testPass = "eyunewumzlywdicd"
	testTo   = "applejava@qq.com"
)

var testUrl = fmt.Sprintf("smtp://%s:%s?user=%s&pass=%s", testHost, testPort, testFrom, testPass)

func ExampleConn_Send() {

	const content = "From: " + testFrom + "\n" +
		"To: " + testTo + "\n" +
		"Subject: test subject\n" +
		"\n" +
		"test body line1.\n" +
		"test body line2.\n"

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	conn, err := Dial(ctx, testHost+":"+testPort, smtp.PlainAuth("", testFrom, testPass, testHost))
	if err != nil {
		panic(err)
	}

	if err := conn.Send(ctx, testFrom, []string{testTo}, []byte(content)); err != nil {
		fmt.Println(err)
	}

	if err := conn.Send(ctx, testFrom, []string{testTo}, []byte(content)); err != nil {
		fmt.Println(err)
	}

	// Output:
}
