package email

import (
	"fmt"
)

// 修改后的QEncoding/BEncoding，编码后的长度超过75时，使用"\n "分隔。

func ExampleWordEncoder_QEncoding() {
	const testContent = "我们中国人民从此站立起来了！We Chinese People Stand Up From Now!"
	fmt.Println(QEncoding.Encode("utf-8", testContent))
	fmt.Println(QEncoding.Encode("utf-8", "?= =?"))
	// Output:
	// =?utf-8?q?=E6=88=91=E4=BB=AC=E4=B8=AD=E5=9B=BD=E4=BA=BA=E6=B0=91=E4=BB=8E?=
	//  =?utf-8?q?=E6=AD=A4=E7=AB=99=E7=AB=8B=E8=B5=B7=E6=9D=A5=E4=BA=86=EF=BC=81?=
	//  =?utf-8?q?We_Chinese_People_Stand_Up_From_Now!?=
	// ?= =?
}

func ExampleWordEncoder_BEncoding() {
	const testContent = "我们中国人民从此站立起来了！We Chinese People Stand Up From Now!"
	fmt.Println(BEncoding.Encode("utf-8", testContent))
	fmt.Println(BEncoding.Encode("utf-8", "?= =?"))
	// Output:
	// =?utf-8?b?5oiR5Lus5Lit5Zu95Lq65rCR5LuO5q2k56uZ56uL6LW35p2l5LqG77yBV2Ug?=
	//  =?utf-8?b?Q2hpbmVzZSBQZW9wbGUgU3RhbmQgVXAgRnJvbSBOb3ch?=
	// ?= =?
}
