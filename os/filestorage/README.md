# filestorage
- upload/store/download files, and clean files that are not linked to any objects.

## Features
- Sync files to multiple machines using scp command.
- Download files using Nginx "X-Accel-Redirect" or sent file directly in response body.
- Clean files that are not linked to any object.


