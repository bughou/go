// this package must be imported first,
// to ensure it is initialized before all the other package.
// so it can print out the real earliest starting time.
package init

import (
	"log"

	"gitee.com/go-better/dev/config"
)

func init() {
	log.Printf(`starting.(%s)`, config.Env())
}
