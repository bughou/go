package postgres

import (
	"database/sql"
	"log"
	"sync"
	"time"

	"gitee.com/go-better/dev/config"
	"gitee.com/go-better/dev/config/db/dburl"
	"gitee.com/go-better/dev/db/pg"
	_ "github.com/lib/pq"
)

var dbs = struct {
	sync.Mutex
	m map[string]*pg.DB
}{m: make(map[string]*pg.DB)}

func DefaultDB() *pg.DB {
	return DB("default")
}

func DB(name string) *pg.DB {
	return Get(config.Get("postgres").GetString(name))
}

func Get(dbAddr string) *pg.DB {
	dbs.Lock()
	defer dbs.Unlock()

	db := dbs.m[dbAddr]
	if db == nil {
		db = New(dbAddr)
		dbs.m[dbAddr] = db
	}
	return db
}

func New(dbAddr string) *pg.DB {
	dbUrl := dburl.Parse(dbAddr)
	db, err := sql.Open("postgres", dbUrl.URL.String())
	if err != nil {
		log.Panic(err)
	}
	if err := db.Ping(); err != nil {
		log.Panic(err)
	}
	db.SetMaxOpenConns(dbUrl.MaxOpen)
	db.SetMaxIdleConns(dbUrl.MaxIdle)
	db.SetConnMaxLifetime(dbUrl.MaxLife)
	return pg.New(db, 5*time.Second)
}
