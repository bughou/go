package config

import (
	"io"
	"log"
	"os"
	"path/filepath"

	"gitee.com/go-better/dev/debug/alarm"
	"gitee.com/go-better/dev/debug/logger"
	"gitee.com/go-better/dev/net/email"
	"gitee.com/go-better/dev/os/fs"
)

const isRun = "isRun"

var theMailer = getMailer()
var theAlarm = getAlarm()
var theLogger, theHttpLogger *logger.Logger

func Mailer() *email.Client {
	return theMailer
}
func Alarm() *alarm.Alarm {
	return theAlarm
}

func Logger() *logger.Logger {
	if theLogger == nil {
		theLogger = NewLoggerFromWriter(os.Stderr)
	}
	return theLogger
}

func HttpLogger() *logger.Logger {
	if theHttpLogger == nil {
		if os.Getenv(isRun) != `` {
			theHttpLogger = NewLoggerFromWriter(os.Stdout)
		} else {
			theHttpLogger = NewLogger("http.log")
		}
	}
	return theHttpLogger
}

func NewLogger(paths ...string) *logger.Logger {
	file, err := fs.NewLogFile(filepath.Join(
		append([]string{Root(), `log`}, paths...)...,
	))
	if err != nil {
		Logger().Fatal(err)
	}
	return NewLoggerFromWriter(file)
}

func NewLoggerFromWriter(writer io.Writer) *logger.Logger {
	loger := logger.New(writer)
	loger.SetAlarm(Alarm())
	if os.Getenv(isRun) != `` {
		loger.SetLevel(logger.Debug)
	} else {
		loger.Set("project", Name())
		loger.Set("env", Env().String())
	}
	return loger
}

func Protect(fn func()) {
	defer Logger().Recover()
	fn()
}

func getMailer() *email.Client {
	m, err := email.NewClient(theConfig.Mailer)
	if err != nil {
		log.Panic(err)
	}
	return m
}
func getAlarm() *alarm.Alarm {
	return alarm.New(alarm.MailSender{
		Receivers: Keepers(),
		Mailer:    Mailer(),
	}, nil, alarm.SetPrefix(DeployName()))
}
