package paging_test

import (
	"fmt"

	"gitee.com/go-better/dev/db/paging"
	"gitee.com/go-better/dev/db/pg"
	"gitee.com/go-better/dev/debug/errs"
)

var db *pg.DB

type Result struct {
	Rows []Row `json:"rows"`
	*paging.Paging
}

type Row struct {
	Id   int64
	Name string
}

func List(page, size string) (*Result, error) {
	result := Result{Paging: paging.New(page, size)}

	var mainSql = fmt.Sprintf("FROM users WHERE type = %d", 1)
	if err := db.Query(&result.Rows, fmt.Sprintf(
		"SELECT id, name %s %s", mainSql, result.Paging.SQL(),
	)); err != nil {
		return nil, errs.Trace(err)
	}
	if err := result.SetupTotalSize(len(result.Rows), db, "SELECT count(*) "+mainSql); err != nil {
		return nil, errs.Trace(err)
	}

	return &result, nil
}

func List2(page, size string) (*Result, error) {
	result := Result{
		Paging: paging.New(page, size, paging.Option{DefaultPageSize: 10, MaxPageSize: 100}),
	}

	var mainSql = fmt.Sprintf("FROM users WHERE type = %d", 1)
	if err := db.Query(&result.Rows, fmt.Sprintf(
		"SELECT id, name %s %s", mainSql, result.Paging.SQL(),
	)); err != nil {
		return nil, errs.Trace(err)
	}

	if result.CurrentPage == 1 && len(result.Rows) < int(result.PageSize) {
		result.SetTotalSize(len(result.Rows))
	} else {
		if err := db.Query(result, "SELECT count(*) "+mainSql); err != nil {
			return nil, errs.Trace(err)
		}
	}

	return &result, nil
}
