module gitee.com/go-better/dev

go 1.16

require (
	github.com/fatih/color v1.13.0
	github.com/garyburd/redigo v1.6.3
	github.com/gocql/gocql v1.0.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/lib/pq v1.10.4
	github.com/mattn/go-runewidth v0.0.13
	github.com/olekukonko/tablewriter v0.0.5
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/cast v1.4.1
	github.com/spf13/cobra v1.4.0
	github.com/tealeg/xlsx v1.0.5
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c
	golang.org/x/text v0.3.7
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v2 v2.4.0
)
