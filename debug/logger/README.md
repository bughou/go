# logger
a logger that integrate with alarm.

## Usage
```go
logger := New(os.Stdout)

logger.SetLevel(Debug)
logger.Debug("the ", "message")
logger.Debugf("this is %s", "test")

logger.Info("the ", "message")
logger.Infof("this is a %s", "test")

logger.Error("err")
logger.Errorf("test %s", "errorf")

logger.Panic("panic !!")
logger.Panicf("test %s", "panicf")

logger.Fatal("fatal !!")
logger.Fatalf("test %s", "fatalf")

defer logger.Recover()

logger.Record(func(ctx context.Context) error {
  // work to do goes here
  return nil
}, nil, func(f *Fields) {
  f.With("key1", "value1").With("key2", "value2")
})
```

