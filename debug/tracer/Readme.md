# tracer
a tracer for golang.

## Usage
```go
func main() {
  ctx := tracer.Start(context.Background(), "main")
  defer tracer.Finish(ctx)

  tracer.Tag(ctx, "key", "value")
  tracer.Logf(ctx, "%v %v", time.Now(), "event")
 
  fmt.Println(tracer.Get(ctx))
}
```

