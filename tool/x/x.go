package main

import (
	"errors"
	"fmt"
	"os"

	"github.com/fatih/color"
	"gitee.com/go-better/dev/os/cmd"
	"gitee.com/go-better/dev/tool/x/access"
	"gitee.com/go-better/dev/tool/x/misc"
	"gitee.com/go-better/dev/tool/x/new"
	"gitee.com/go-better/dev/tool/x/release"
	"gitee.com/go-better/dev/tool/x/services"
	"github.com/spf13/cobra"
)

const moduleVersion = `v1.1.3`
const fullVersion = moduleVersion + ` 20220207`

func main() {
	// log.SetFlags(log.Lshortfile | log.LstdFlags)
	color.NoColor = false
	cobra.EnableCommandSorting = false
	root := &cobra.Command{
		Use:   `x`,
		Short: `Be small and beautiful.`,
	}
	root.PersistentFlags().SortFlags = false

	root.AddCommand(new.Cmd(moduleVersion))
	root.AddCommand(access.Cmd())
	root.AddCommand(services.Cmds()...)
	root.AddCommand(misc.Cmds(root)...)
	root.AddCommand(versionCmd(), updateCmd())

	if err := root.Execute(); err != nil {
		os.Exit(1)
	}
}

func versionCmd() *cobra.Command {
	return &cobra.Command{
		Use:   `version`,
		Short: `Show x version.`,
		RunE: release.NoArgCall(func() error {
			fmt.Println(`x ` + fullVersion)
			return nil
		}),
	}
}

func updateCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     `update [version]`,
		Short:   `Update to lastest version.`,
		Example: `x update v1.0.1`,
		RunE: func(c *cobra.Command, args []string) error {
			target := `gitee.com/go-better/dev/tool/x`
			switch len(args) {
			case 0:
			case 1:
				target += `@` + args[0]
			default:
				return errors.New(`more than one arguments given.`)
			}

			fmt.Println(`current version ` + fullVersion)
			if err := release.GoGetByProxy(`-u`, target); err != nil {
				return err
			}
			_, err := cmd.Run(cmd.O{}, `x`, `version`)
			return err
		},
	}

	return cmd
}
