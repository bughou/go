package godoc

import "gitee.com/go-better/dev/os/cmd"

func shell() error {
	_, err := cmd.Run(cmd.O{}, `docker`, `exec`, `-it`,
		`--detach-keys=ctrl-@`, `workspace-godoc`, `bash`,
	)
	return err
}

func ps() error {
	_, err := cmd.Run(cmd.O{}, `docker`, `ps`, `-f`, `name=workspace-godoc`)
	return err
}
