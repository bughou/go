package deploy

import (
	"gitee.com/go-better/dev/config/config"
	"gitee.com/go-better/dev/tool/x/release"
)

func GetCommonArgs(svcName, env, tag string) []string {
	args := []string{`-e`, config.EnvVar + `=` + env}

	service := release.GetService(env, svcName)
	args = append(args, service.Options...)
	args = append(args, service.ImageName(tag))
	args = append(args, service.Command...)
	return args
}
