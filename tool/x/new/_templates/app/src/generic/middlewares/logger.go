package middlewares

import (
	"gitee.com/go-better/dev/config"
	"gitee.com/go-better/dev/net/goa/middlewares"
)

var Logger = middlewares.NewLogger(config.HttpLogger())
