package middlewares

import (
	"gitee.com/go-better/dev/net/goa"
	"gitee.com/go-better/dev/debug/tracer"
	"{{ .ModulePath }}/generic/session"
)

func SessionParse(c *goa.Context) {
	parseSession(c)
	c.Next()
}

func parseSession(c *goa.Context) {
	ck, _ := c.Request.Cookie(session.Cookie.Name)
	if ck == nil || ck.Value == "" {
		return
	}
	ck.MaxAge = session.Cookie.MaxAge

	var data session.Session
	if err := session.CookieStore.Get(ck, &data); err != nil {
		tracer.Log(c.Context(), "session: ", err)
		return
	}

	c.Set("session", data)
}
