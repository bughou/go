package middlewares

import (
	"net/url"

	"gitee.com/go-better/dev/net/goa"
	"gitee.com/go-better/dev/net/goa/middlewares"
)

var CORS = middlewares.NewCORS(allowOrigin)

func allowOrigin(origin string, c *goa.Context) bool {
	u, err := url.Parse(origin)
	if err != nil {
		return false
	}
	hostname := u.Hostname()
	return hostname == `localhost`
}
