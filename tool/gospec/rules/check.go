package rules

import (
	"go/ast"
	"strings"

	"gitee.com/go-better/dev/tool/gospec/rules/walker"

	dirPkg "gitee.com/go-better/dev/tool/gospec/rules/objects/dir"
	pkgPkg "gitee.com/go-better/dev/tool/gospec/rules/objects/names/pkg"

	filePkg "gitee.com/go-better/dev/tool/gospec/rules/objects/file"
	funcPkg "gitee.com/go-better/dev/tool/gospec/rules/objects/func"
	structPkg "gitee.com/go-better/dev/tool/gospec/rules/objects/struct"

	constPkg "gitee.com/go-better/dev/tool/gospec/rules/objects/names/const"
	labelPkg "gitee.com/go-better/dev/tool/gospec/rules/objects/names/label"
	typePkg "gitee.com/go-better/dev/tool/gospec/rules/objects/names/type"
	varPkg "gitee.com/go-better/dev/tool/gospec/rules/objects/names/var"
)

// check rules
func Check(dir string, files []string) {
	dirPkg.Check(dir)

	pkg := pkgPkg.NewChecker()
	for _, path := range files {
		isTest := strings.HasSuffix(path, "_test.go")
		w := walker.New(path)

		pkg.Check(w.AstFile.Name, w.FileSet)
		filePkg.Check(isTest, path, w.SrcFile, w.AstFile, w.FileSet)

		w.Walk(func(isLocal bool, node ast.Node) {
			funcPkg.Check(isTest, node, w.FileSet)
			structPkg.Check(node, w.FileSet)

			constPkg.Check(isLocal, node, w.FileSet)
			varPkg.Check(isLocal, node, w.FileSet)
			typePkg.Check(isLocal, node, w.FileSet)
			labelPkg.Check(node, w.FileSet)
		})
	}
}
