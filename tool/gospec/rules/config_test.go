package rules

import (
	"fmt"
	"strings"

	"gitee.com/go-better/dev/debug/deep"
)

func ExampleCheckDefaultConfig() {
	configInCode := config
	config = configT{}
	LoadConfig()
	configInYaml := config

	fmt.Println(strings.Join(deep.Equal(configInYaml, configInCode), "\n"))

	// Output:
}
