package dirpkg

import (
	namepkg "gitee.com/go-better/dev/tool/gospec/rules/name"
)

var Dir = Rule{
	key:  "dir",
	Name: namepkg.Rule{MaxLen: 30, Style: "lower_case"},
	Size: sizeRule{MaxEntries: 20},
}

func Check(path string) {
	Dir.check(path)
}
