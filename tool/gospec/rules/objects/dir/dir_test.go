package dirpkg

import (
	"gitee.com/go-better/dev/tool/gospec/problems"
)

func ExampleCheck() {
	problems.Clear()
	Check("")
	Check("../dir")
	Check(".")
	Check("..")
	problems.Render()
	// Output:
}
