package pkgpkg

import (
	"go/ast"
	"go/token"

	namepkg "gitee.com/go-better/dev/tool/gospec/rules/name"
)

type Rule struct {
	thing, key   string
	namepkg.Rule `yaml:",inline"`
}

func (r Rule) check(name *ast.Ident, fileSet *token.FileSet) {
	r.Exec(name.Name, r.thing, r.key, fileSet.Position(name.Pos()))
}
