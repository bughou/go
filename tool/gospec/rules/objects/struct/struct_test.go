package structpkg

import (
	"go/ast"

	"gitee.com/go-better/dev/tool/gospec/problems"
	"gitee.com/go-better/dev/tool/gospec/rules/walker"
)

func ExampleCheck() {
	var src = `package example
type T struct {
  Name string
  A, B, C int
}
`
	problems.Clear()
	w := walker.Parse("example.go", src)
	w.Walk(func(isLocal bool, node ast.Node) {
		Check(node, w.FileSet)
	})
	problems.Render()
	// Output:
}
