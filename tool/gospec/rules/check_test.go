package rules

import (
	"gitee.com/go-better/dev/tool/gospec/problems"
)

func ExampleCheck() {
	problems.Clear()
	Check(".", []string{"check_test.go"})
	problems.Render()
	// Output:
}
