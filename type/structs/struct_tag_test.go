package structs

import (
	"fmt"
)

func ExampleGetTag() {
	fmt.Println(GetTag(`a:"av"`, `a`))
	// Output:
	// av
}

func ExampleLookupTag_false() {
	fmt.Println(LookupTag(``, ``))
	fmt.Println(LookupTag(` `, ``))
	fmt.Println(LookupTag(`a`, ``))
	fmt.Println(LookupTag(`a:""`, ``))
	fmt.Println(LookupTag(`a:"av"`, `b`))
	// Output:
	//  false
	//  false
	//  false
	//  false
	//  false
}

func ExampleLookupTag_true() {
	fmt.Println(LookupTag(`a:""`, `a`))
	fmt.Println(LookupTag(`a:"av"`, `a`))
	fmt.Println(LookupTag(`a:"av" b:"b\" v"`, `b`))
	fmt.Println(LookupTag(`a:"av"
		b:"b v" `, `b`))
	fmt.Println(LookupTag(`名称:"值
\"def"`, `名称`))
	// Output:
	//  true
	// av true
	// b" v true
	// b v true
	// 值
	// "def true

}

func ExampleParseTag_true() {
	fmt.Println(ParseTag(`a:""`))
	fmt.Println(ParseTag(`a:"av"`))
	fmt.Println(ParseTag(`a:"av" b:"b\" v"`))
	fmt.Println(ParseTag(`a:"av"
		b:"b v" `))
	fmt.Println(ParseTag(`名称:"值
\"def"`))
	// Output:
	// map[a:]
	// map[a:av]
	// map[a:av b:b" v]
	// map[a:av b:b v]
	// map[名称:值
	// "def]
}

func ExampleTrimLeadingSpace() {
	fmt.Printf("%#v\n", trimLeadingSpace("a"))
	fmt.Printf("%#v\n", trimLeadingSpace("  "))
	fmt.Printf("%#v\n", trimLeadingSpace("  b"))
	// Output:
	// "a"
	// ""
	// "b"
}

func ExampleStripName() {
	fmt.Println(stripName("a:xx"))
	name, tag := stripName("a :")
	fmt.Printf("%s%s\n", name, tag)
	fmt.Println(stripName("a:"))
	// Output:
	// a xx
	//
	// a
}

func ExampleStripValue() {
	fmt.Println(stripValue(`"xx"yy`))
	name, tag := stripValue(`a`)
	fmt.Printf("%s%s\n", name, tag)
	name, tag = stripValue(`"a:`)
	fmt.Printf("%s%send\n", name, tag)
	// Output:
	// "xx" yy
	//
	// end
}
