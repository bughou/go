# structs
## 功能
- 递归遍历结构体的字段
- 结构体的标签的查找，支持多行文本的标签。`reflect.StructTag`不支持跨多行文本的标签。
- 打印结构体，不打印零值字段

## functions
- recursively traverse fields of struct.
- lookup struct tag, support tag with multiline text. `reflect.StructTag` does not support tag with multiline text.
- print struct, but do not include zero value fields.

