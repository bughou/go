package time2

import (
	"database/sql/driver"
	"fmt"
	"strings"
	"time"
)

// HMS reprenets hour, minute, second.
type HMS struct {
	time.Time
}

const (
	timeLayout = "15:04:05"
	midnight24 = "24:00:00"
	midnight   = "23:59:59"
)

func NewHMS(str string) (*HMS, error) {
	if str == "" || str == "null" {
		return &HMS{}, nil
	}

	if str == midnight24 {
		str = midnight
	}

	t, err := time.Parse(timeLayout, str)
	if err != nil {
		return nil, err
	}

	return &HMS{t}, nil
}

func (hms HMS) String() string {
	return hms.Format(timeLayout)
}

func (hms *HMS) UnmarshalJSON(b []byte) (err error) {
	str := strings.Trim(string(b), "\"")

	t, err := NewHMS(str)
	if err != nil {
		return err
	}

	*hms = *t
	return nil
}

// OfToday return the time(hour,minute,second) of today.
func (hms HMS) OfToday() time.Time {
	now := time.Now()
	return time.Date(
		now.Year(), now.Month(), now.Day(),
		hms.Hour(), hms.Minute(), hms.Second(), 0, now.Location(),
	)
}

func (hms HMS) MarshalJSON() ([]byte, error) {
	if hms.Time.IsZero() {
		return []byte("null"), nil
	}

	return []byte(fmt.Sprintf("\"%s\"", hms.String())), nil
}

func (hms HMS) Value() (driver.Value, error) {
	return hms.Format(timeLayout), nil
}

func (hms *HMS) Scan(value interface{}) error {
	if value == nil {
		*hms = HMS{}
		return nil
	}

	v, ok := value.(time.Time)
	if ok {
		*hms = HMS{v}
		return nil
	}
	return fmt.Errorf("can not convert %v to hms", value)
}
