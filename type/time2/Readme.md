# time2
time2是对标准库time包的增强。包括：
- 增加符合中文习惯的Time类型。
- 增加日期类型：Date。
- 增加时分秒类型：HMS。
- 增强Duration类型：增加对年、月、日时间单位的支持，内置中英文时间单位，支持时间单位自定义。


