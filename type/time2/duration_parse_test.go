package time2

import (
	"fmt"
)

func ExampleParseDuration_empty() {
	fmt.Println(ParseDuration(``))
	fmt.Println(ParseDuration(`0`))
	fmt.Println(ParseDuration(`+0`))
	fmt.Println(ParseDuration(`-0`))

	// Output:
	// 0 <nil>
	// 0 <nil>
	// 0 <nil>
	// 0 <nil>
}

func ExampleParseDuration_EN() {
	fmt.Println(ParseDuration(`123s`))
	fmt.Println(ParseDuration(`year`))
	fmt.Println(ParseDuration(`1Y2M3D4h306s`))
	fmt.Println(ParseDuration(`1.1m`))
	fmt.Println(ParseDuration(`-1.1m`))
	fmt.Println(ParseDuration(`9D`))

	// Output:
	// 2m3s <nil>
	// 1Y <nil>
	// 1Y2M3D4h5m6s <nil>
	// 1m6s <nil>
	// -1m6s <nil>
	// 9D <nil>
}

func ExampleParseDuration_ZH() {
	fmt.Println(ParseDuration(`123秒`))
	fmt.Println(ParseDuration(`年`))
	fmt.Println(ParseDuration(`1年2月3日4时306秒`))
	fmt.Println(ParseDuration(`1.1分`))
	fmt.Println(ParseDuration(`-1.1分`))

	// Output:
	// 2分3秒 <nil>
	// 1年 <nil>
	// 1年2个月3天4小时5分6秒 <nil>
	// 1分6秒 <nil>
	// -1分6秒 <nil>
}

/*
func ExampleTime_ParseDuration() {
	fmt.Println(time.ParseDuration(`1h3m5`))
	// Output:
	// 0s time: missing unit in duration 1h3m5
}
*/
