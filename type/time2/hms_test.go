package time2

import (
	"encoding/json"
	"fmt"
)

func ExampleHMS() {
	type Stu struct {
		Name       string `json:"name"`
		FinishedAt HMS    `json:"finishedAt"`
	}

	d, err := NewHMS("14:05:00")
	fmt.Println(err)

	stu := Stu{Name: "A", FinishedAt: *d}
	b, _ := json.Marshal(stu)
	fmt.Println(string(b))

	stu = Stu{Name: "A"}
	b, _ = json.Marshal(stu)
	fmt.Println(string(b))

	stu2 := Stu{}
	data := []byte(`{"name": "W5"}`)
	json.Unmarshal(data, &stu2)
	fmt.Println(stu2.FinishedAt.IsZero())

	data = []byte(`{"name": "W5", "finishedAt": ""}`)
	json.Unmarshal(data, &stu2)
	fmt.Println(stu2.FinishedAt)

	fmt.Println(NewHMS("24:00:00"))

	// Output:
	// <nil>
	// {"name":"A","finishedAt":"14:05:00"}
	// {"name":"A","finishedAt":null}
	// true
	// 00:00:00
	// 23:59:59 <nil>
}
