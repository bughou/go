package time2

import (
	"encoding/json"
	"fmt"
)

func ExampleParseDate() {
	fmt.Println(ParseDate("2018-04-01"))
	// Output: 2018-04-01 <nil>
}

func ExampleDate_MarshalJSON() {
	var day = Date{}
	b, err := json.Marshal(day)
	fmt.Println(string(b), err)

	day = NewDate(2018, 04, 01)
	b, err = json.Marshal(day)
	fmt.Println(string(b), err)
	// Output:
	// null <nil>
	// "2018-04-01" <nil>
}

func ExampleDate_UnmarshalJSON() {
	var day = Date{}
	err := json.Unmarshal([]byte(`"2019-09-12"`), &day)
	fmt.Println(day, err)

	day = Date{}
	err = json.Unmarshal([]byte(`2019-09-12`), &day)
	fmt.Println(day, err)

	// Output:
	// 2019-09-12 <nil>
	//  invalid character '-' after top-level value
}

func ExampleDate_Value() {
	b, err := Date{}.Value()
	fmt.Println(string(b.([]byte)), err)

	day2 := NewDate(2018, 04, 01)
	b, err = day2.Value()
	fmt.Println(string(b.([]byte)), err)

	// Output:
	// NULL <nil>
	// '2018-04-01' <nil>
}

func ExampleDate_compare() {
	day1 := NewDate(2021, 10, 1)
	day2 := NewDate(2021, 10, 2)
	day3 := NewDate(2021, 10, 2)
	fmt.Println(day1.After(day2))
	fmt.Println(day1.Before(day2))
	fmt.Println(day1.Equal(day2))
	fmt.Println(day2.After(day3))
	fmt.Println(day2.Before(day3))
	fmt.Println(day2.Equal(day3))

	// Output:
	// false
	// true
	// false
	// false
	// false
	// true
}
