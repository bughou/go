package sortedset

import (
	"reflect"
	"sort"
)

func RemoveInt64(s []int64, target int64) []int64 {
	if len(s) == 0 {
		return s
	}

	i := sort.Search(len(s), func(i int) bool {
		return s[i] >= target
	})

	if i < len(s) && s[i] == target {
		return append(s[:i], s[i+1:]...)
	}
	return s
}

func RemoveString(s []string, target string) []string {
	if len(s) == 0 {
		return s
	}

	i := sort.Search(len(s), func(i int) bool {
		return s[i] >= target
	})

	if i < len(s) && s[i] == target {
		return append(s[:i], s[i+1:]...)
	}
	return s
}

func RemoveValue(s, target reflect.Value, fields ...string) reflect.Value {
	if !s.IsValid() || s.Len() == 0 {
		return s
	}

	i := sort.Search(s.Len(), func(i int) bool {
		return CompareValue(s.Index(i), target, fields...) >= 0
	})

	if i < s.Len() && CompareValue(s.Index(i), target, fields...) == 0 {
		return reflect.AppendSlice(s.Slice(0, i), s.Slice(i+1, s.Len()))
	}
	return s
}
