package sortedset

import (
	"reflect"
	"sort"

	"gitee.com/go-better/dev/type/slice"
)

func SaveInt64(s []int64, target int64) []int64 {
	if len(s) == 0 {
		return []int64{target}
	}

	i := sort.Search(len(s), func(i int) bool {
		return s[i] >= target
	})

	if i >= len(s) { // not found
		return append(s, target)
	}
	if s[i] == target {
		return s
	}
	return slice.InsertInt64(s, i, target)
}

func SaveString(s []string, target string) []string {
	if len(s) == 0 {
		return []string{target}
	}

	i := sort.Search(len(s), func(i int) bool {
		return s[i] >= target
	})

	if i >= len(s) { // not found
		return append(s, target)
	}
	if s[i] == target {
		return s
	}
	return slice.InsertString(s, i, target)
}

func SaveValue(s, target reflect.Value, fields ...string) reflect.Value {
	if !s.IsValid() {
		return reflect.Append(reflect.MakeSlice(reflect.SliceOf(target.Type()), 0, 1), target)
	}
	if s.Len() == 0 {
		return reflect.Append(s, target)
	}

	i := sort.Search(s.Len(), func(i int) bool {
		return CompareValue(s.Index(i), target, fields...) >= 0
	})

	if i >= s.Len() { // not found
		return reflect.Append(s, target)
	}
	if CompareValue(s.Index(i), target, fields...) == 0 {
		s.Index(i).Set(target)
		return s
	}
	return slice.InsertValue(s, i, target)
}
